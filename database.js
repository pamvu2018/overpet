//Setting of database

let mongoose = require('mongoose');
const databaseUrl = 'mongodb+srv://Schitzanka:test12@cluster0.rojfv.mongodb.net/test?retryWrites=true&w=majority'



async function connect() {
  try {
    const dbClient = await mongoose.connect(databaseUrl)
    console.log('Database connection successful')
    return dbClient
  }
  catch (error){
    console.error('Database connection error: ', error)
  }

}


module.exports = {
  databaseUrl,
  connect
}