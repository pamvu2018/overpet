let pets=[]
async function updatePet(petData) {
    try {

        const result = await axios.patch(`/update-pets`,petData)
    }
    catch (error){
        console.lod(error)
    }
}

async function deletePet(petId) {
    try {

        const result = await axios.delete(`/delete-pets/${petId}`)
    }
    catch (error){
        console.lod(error)
    }
}

async function fetchPets() {

    try {
        const result = await axios.get(`/list-pets`)
        pets=result.data

        if(typeof pets === 'string') {
            window.location.replace('/login')
            return
        }
        let htmlPetsTemplate = ''
        pets.forEach(pet => {
          htmlPetsTemplate+=`
            <div class="pet-card">
                <h3>${pet.petname}</h3>
                <p>Age: ${pet.petage}</p>
                <p>Breed: ${pet.petbreed}</p>
                <button type="button" data-pet="${pet._id}" onclick="upsertMyPets(event)">Update pet</button>
                <button type="button"  onclick="deletePet('${pet._id}')">Delete pet</button>
            </div>`
        });
       const petContainer= document.getElementById('pets-container')
       petContainer.innerHTML=htmlPetsTemplate
       
    }
    catch (error) {
        console.log("There is an error: ", error)
    }
}


function savePets(newPet){
    
    let xhr = new XMLHttpRequest()
        xhr.open('POST','/save-pets')
        xhr.setRequestHeader('Content-Type','application/json')
        xhr.send(JSON.stringify( newPet ))

        xhr.onload = ()=>{
            console.log(xhr.response)
        }
}


function upsertMyPets(event) {

const petId = event.target.getAttribute('data-pet')
console.log(petId)
const selectedPet = pets.find(pet=> pet._id === petId)
console.log(selectedPet)

        let petCard=document.createElement("div")
        petCard.id="card-div"
        petCard.innerText='Enter your pets data'

        let petSection=document.createElement("section")
        // petSection.setAttribute("method", "post")
        // petSection.setAttribute("action", "/save-pets")
        
        let petName = document.createElement("input")
        petName.type = "text"
        petName.name="petname"
        petName.placeholder= "Pet name"
        petName.value= selectedPet?.petname || ''

        let petAge = document.createElement("input")
        petAge.type="text"
        petAge.name="petage"
        petAge.placeholder="Pet age"
        petAge.value= selectedPet?.petage || ''
        
        let petBreed = document.createElement("input")
        petBreed.type = "text"
        petBreed.name = "petbreed"
        petBreed.placeholder = "Pet breed"
        petBreed.value= selectedPet?.petbreed || ''
        
        let submitButton = document.createElement("button");
        // submitButton.type = 'submit'
        // submitButton.value = 'Submit'
        submitButton.id = 'create-pet'
        submitButton.innerText = 'create pet'
        
        petSection.append(petName,petAge,petBreed,submitButton)
        petCard.appendChild(petSection)
        document.querySelector('main').appendChild(petCard)

        submitButton.onclick=()=> {
            if(!petId){
                
                const newPet ={
                    petname: petName.value,
                    petage: petAge.value,
                    petbreed: petBreed.value
                }
                savePets(newPet)
            } else {
               
                const updatePetObj ={
                    id:petId,
                    petname: petName.value,
                    petage: petAge.value,
                    petbreed: petBreed.value
                }
                console.log(this)
                updatePet(updatePetObj)
            }

        }
    


}

fetchPets()