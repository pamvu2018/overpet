function addReceipts() {
    let addCurrentReceipt=document.getElementById("add-receipt")
    console.log(addCurrentReceipt)
   

    addCurrentReceipt.onclick =() => {
        let receiptForm=document.createElement("form")
        receiptForm.id="upload-form"
        receiptForm.enctype='multipart/form-data'
        receiptForm.setAttribute("method", "post")
        receiptForm.setAttribute("action", "/uploadReceipt")
        console.log("My receipts are going to be uploaded")
       
        let receiptInput=document.createElement("input")
        receiptInput.type="file"
        receiptInput.id="my-File"
        receiptInput.name="receipt"

        let submitInput=document.createElement("input")
        submitInput.type="submit"
        receiptForm.appendChild(receiptInput)
        receiptForm.appendChild(submitInput)
        document.querySelector('main').appendChild(receiptForm)

    }
}

function base64ToBlob( base64, type = "application/octet-stream" ) {
    const binStr = atob( base64 );
    const len = binStr.length;
    const arr = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      arr[ i ] = binStr.charCodeAt( i );
    }
    return new Blob( [ arr ], { type: type } );
}

async function renderPdf(fileName) {
    const result = await axios.get(`read-file/${fileName}`)

    document.body.innerHTML = `
        <object>
            <embed id="pdfID" type="text/html" width="1200" height="600" src="${result.data.imgurl}" />
        </object>
    `
}


async function fetchPdfDocs() {
    // TODO: implement axios get with endpoint called /get-pdf-files
    // render them in the table as a button that opens a modal as soon you clicked
    // and the same click will fetch the base 64 of this pdf file
}



fetchPdfDocs()
addReceipts()
// renderPdf('108f24bba015f39c53bb04dae2d5596b')