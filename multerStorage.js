// Setting of multer storage

const multer = require('multer');
const {GridFsStorage} = require('multer-gridfs-storage');
const { databaseUrl } = require('./database');

async function startMulterConnection() {
    const storage = new GridFsStorage({
        url: databaseUrl,
        file: (req, file) => {
            if (file.mimetype === 'image/jpeg') {
                return {
                    bucketName: 'photos',
                };
            } else {
                return null
            }
        }
    });
    console.log('File storage connection successful')

    const upload = multer({ storage });
    return upload
}



module.exports = startMulterConnection