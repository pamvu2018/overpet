const passport = require('passport')
const express = require('express')
const expressSession = require('express-session')
const LocalStrategy = require('passport-local')
const UserSchema = require('./models/User')
const PetSchema = require('./models/Pets')
const {connect} = require("./database")
const { ensureLoggedIn } = require('connect-ensure-login')
const fsFileModel = require('./models/FsFiles')
const fsChunkModel = require('./models/FsChunks')
const startMulterConnection = require('./multerStorage')
const petModel = require('./models/Pets.js')

async function startServer() {
    await connect()
    const upload = await startMulterConnection()


    const app = express()
    app.use(express.static('view'))

    // Passport.js for authentification
    passport.serializeUser(UserSchema.serializeUser())
    passport.deserializeUser(UserSchema.deserializeUser())
    passport.use(new LocalStrategy((UserSchema.authenticate())))

    app.use(expressSession({
        secret: '&6r/dbpvSbL0ve',
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 3 * 60 * 1000
        }
    }))


    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))

    app.use(passport.initialize())
    app.use(passport.session())

    //Registration and creation of user with Mongoose user schema
    app.post('/register', async (request, response) => {
        console.log(request.body)
        try {
            const newUser = await UserSchema.create({
                username: request.body.username,
                email: request.body.email,
                password: request.body.password

            })
            const savedUser = await newUser.save()
            response.redirect(request.get('referer'))
        } catch (error) {
            response.status(400).send(error)
        }
    })

    //Logout and redirection to login page
    app.get('/logout', (request, response) => {
        request.logout()
        response.redirect('view/login.html')
    })

    //Creation of pet according to made Mongoose pet schema
    app.post('/save-pets',ensureLoggedIn("/login"), async (req, res) => {

        const newPet = await PetSchema.create({
            petname: req.body.petname,
            petage: req.body.petage,
            petbreed: req.body.petbreed,
            userId:req.user._id.toString()
        })

        const savedPet = await newPet.save()
        res.send(savedPet)
    })

    
    app.post('/update-pets', async (req, res) => {

        console.log()

    })

    // Receipts posting with ensure login
    app.post('/uploadReceipt', ensureLoggedIn("/login"), upload.single('receipt'), async (req, res) => {
        console.log(req.body)
        res.redirect('/receipts.html')
    })

    app.get("/", ensureLoggedIn("/login"), (req, res) => {
        res.sendFile(__dirname + '/view/main.html')
    })

    //Listing of created pets
    app.get("/list-pets",ensureLoggedIn("/login"),async (req,res)=>{
        try{
            const result = await petModel.find({userId:req.user._id.toString()})
            res.send(result)
        } 
        catch {
            res.status(500).send('Something went wrong while searching your database')
        }

    })

    //Update of pre-existing pet
    app.patch("/update-pets",ensureLoggedIn("/login"),async (req,res)=>{
        const {id,...rest}=req.body
        try{
            const result = await petModel.findOneAndUpdate({_id:id},{
                ...rest
            })
            res.send(result)
        } 
        catch {
            res.status(500).send('Something went wrong while searching your database')
        }

    })

    //Remove created pet
    app.delete("/delete-pets/:petId",ensureLoggedIn("/login"),async (req,res)=>{
        try{
            const result = await petModel.findOneAndDelete({_id:req.params.petId})
            res.send(result)
        } 
        catch {
            res.status(500).send('Something went wrong while deleting your pet object')
        }

    })

    //Multer and data bank
    // app.get("/read-file/:filename", async (req, res) => {
    //     fsFileModel.find({ filename: req.params.filename }).exec((err, docs) => {

    //         if (err) {
    //             return res.send({ title: 'File error', message: 'Error finding file', error: err.errMsg });
    //         }
    //         if (!docs || docs.length === 0) {
    //             return res.send({ title: 'Download Error', message: 'No file found' });
    //         } else {
    //             //Retrieving the chunks from the db
    //             fsChunkModel.find({ files_id: docs[0]._id }).sort({ n: 1 }).exec(function (err, chunks) {
    //                 if (err) {
    //                     return res.send({ title: 'Download Error', message: 'Error retrieving chunks', error: err.errmsg });
    //                 }
    //                 if (!chunks || chunks.length === 0) {
    //                     //No data found
    //                     return res.send({ title: 'Download Error', message: 'No data found' });
    //                 }
    //                 //Append Chunks
    //                 let fileData = [];
    //                 for (let i = 0; i < chunks.length; i++) {
    //                     //This is in Binary JSON or BSON format, which is stored
    //                     //in fileData array in base64 endocoded string format
    //                     fileData.push(chunks[i].data.toString('base64'));
    //                 }
    //                 //Display the chunks using the data URI format
    //                 let finalFile = 'data:' + docs[0].contentType + ';base64,' + fileData.join('');
    //                 res.send({ title: 'Image File', message: 'Image loaded from MongoDB GridFS', imgurl: finalFile });
    //             });
    //         }


    //     })
    // })

    //without authentification route
    app.get("/login", (req, res) => {
        res.sendFile(__dirname + '/view/login.html')

    })
    app.post(
        '/login', async (request, response) => {
            console.log(request.body)
            const user = await UserSchema.findOne({ username: request.body.username })
            console.log(user)
            if (!user) {
                response.status(401).send()
                return
            }
            if (request.body.password !== user.password) {
                response.status(401).send()
                return
            }
            request.login(user, function (error) {
                if (error) {
                    response.redirect(request.get('referer'))
                    return
                }
                return response.redirect('/');
            });

        }
    )

    //Welcome page
    app.get('/welcome', (request, response) => {
        response.sendFile(__dirname + '/view/index.html')
    })

    app.get('*', (request, response) => {
        response.status(404)
    })


    app.listen(5000, () => {
        console.log('Server is listening to http://localhost:5000')
    })
}

startServer()