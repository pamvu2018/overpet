const mongoose = require('mongoose')
const PetSchema = new mongoose.Schema(
     {

     petname: {
        type: String,
        required:true
     },
     petage: {
         type: String,
         required:true
     },
     petbreed: {
         type:String,
         required:true
     },
     userId: {
        type: String,
        required:true
     }
 });

module.exports = mongoose.model('Pet', PetSchema)


 