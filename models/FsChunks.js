const { ObjectId } = require('mongodb')
const mongoose = require('mongoose')
const FsChunks = new mongoose.Schema(
    {
        files_id: {
            type: ObjectId
        },
        n: {
            type: Number
        },
        data: {
            type: Buffer
        }
    });

module.exports = mongoose.model('fs.chunks', FsChunks)